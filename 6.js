/*
    *Promise yapısı --> Birden fazla fonksiyonun arka arkadaya çalışmasını sağlamak için ya callback yada promise yapısı
    kullanılması gerekiyor.Callback ile okunabilirlik bir tık daha zor olduğu için alternatif olarak promise yapısı
    inşaedilmiştir.

    -------------------------------------------------------------
    Callback'lerde okunabilirlik biraz daha zor
    const sayHello = function(data,callback){
    console.log(data);
    callback(data);

    sayHello("Erdemstar",function (data) {
        console.log(data+1);
        sayHello("Erdem",function (data) {
            console.log(data+2)
        });
    });

    -------------------------------------------------------------
    
    Kod'daki her bir then ifadesi yukarıdaki callback fuction'a denk geliyor.

    const helWord = function(data){
    return new Promise((resolve,reject) => {
        resolve("Everything is okay");        
    });
    
    helWord()
        .then((data)=>{
            console.log(data);
            return `${data} ${data}`;
        })
        .then((data) => {
            console.log(data);
            return `${data} ${data} ${data}`;
        })
        .then((data) => {
            console.log(data);
        });





    Tam olarak farkını anla 
*/



const pair = function pairNumber(say){
    return new Promise(function (resolve,reject){
        if (say % 2 == 0){
            resolve(say+2);
        }
        else{
            reject("Sayı çift değildir");
        }
    });
}
pair(0)
    .then((say) => {
        console.log(say);
        return say+2;
    })
    .then((say) =>{
        console.log(say);
        return say+1;
    })
    .catch((error) => {
        console.log(error);
    });


