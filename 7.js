/*
    *async-await => Sıralı fonksiyonları ard arda çağırmak için , callback , promise yada async-await yapısı 
    kullanılabilir.Aşağıda userIDS'den alınana rastgele id'ye göre username array'den isim dönülmektedir.Bunu
    sıralı bir şekilde (sekron) bir şekilde yapılması gerektiği için promise kullanıldı.Daha fazla okunaklı hale
    gelmesi için async kullanıldı.Bu tanımalama sayesinde örnek vermek gerekirse 40.satırdaki kod çalıştırılmadan
    41.satırdaki kod çalıştırılmayacaktır.




    // Aşağıda a adlı bir obje oluşturuldu.Kendi içiersinde name ve surname property'leri var
    const a = {name: "Erdem",surname: "Star"};
    console.log(a.name , a.surname);
 */

//callback

let userIDS = [1, 2, 3, 4, 5];
let username = [[1, "Erdem"], [2, "Ali"], [3, "Veli"], [4, "Mert"], [5, "Alp"]];

const randomUID = function () {
    let item = userIDS[Math.floor(Math.random() * userIDS.length)];
    return item;
}

const writeName = function (id) {
    username.forEach(element => {
        if (id == element[0]) {
            console.log(element[1]);
        }
    });
}




// promise chain
const userPromise = function () {
    return new Promise(function (resolve, reject) {
        resolve("Başarılı");
    })
};

/*
userPromise()
    .then(function () {
        return userIDS[Math.floor(Math.random() * userIDS.length)];
    })
    .then(function (id) {
        username.forEach(element => {
            if (id == element[0]) {
                console.log(element[1]);
            }});
    });
*/

async function DogruAkis() {
    /*Burada PHP'deki gibi satır satır çalışacak olan fonksiyon veya kod parçacıklarının tanımlandıkları yer.NodeJS
    yapısı gerekği non blockıng olduğu için kodlar çalışma hızlarına göre çağırılırlar.Biz burada nodeJS'e benim
    belirlediğim sırada çalış demesini async kullanarak yapıyoruz.Promise'ın okunabilrilik olarak alternatifidir.Aynı
    mantıkla çalışır.Bu yapıda hata yakılamk için try-catch blokları kullanılır*/
    try {
        const id = await randomUID();
        await writeName(id);
    }
    catch (error) {

    }



}
DogruAkis();