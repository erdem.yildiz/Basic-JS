/**
 * event-emitter => Nodejs içersinde , node içersindeki olay yönteisicis sayesinde herhnangi bir dosya okudunduğunda 
 * db üzerinde işlemer yapıldığında kısımlarda çalışıan bir yapıdır.Böylelikle olaylar daha kolay yönetilebilir.
 * Genel mantık -> olay tanımlanır daha sonra istenilen kadar olay tetikle
 * 
 */

const events = require("events");
const eventEmiter = new events.EventEmitter();

// bir olay tanımalak için 
eventEmiter.on('Selam', function () {
    console.log("Merhaba Node");
});

eventEmiter.emit('Selam'); // bir olayı tetiklemek için trigger

//--------------------------------------------------


// parametreli bir olay tanımalak için 
eventEmiter.on('Selamdur', function (name) {
    console.log(`Merhaba ${name}`);
});

eventEmiter.emit('Selamdur', "Erdemstar"); // bir olayı tetiklemek için trigger

//--------------------------------------------------


// objeli bir olay tanımalak için 
eventEmiter.on('Selamdur2', function (obj) {
    console.log(`Merhaba ${obj.name} ${obj.surname}`);
});

eventEmiter.emit('Selamdur2', { name: "Erdem", surname: "kıral" }); // bir olayı tetiklemek için trigger



//--------------------------------------------------


// Bazı durumlarda bir event'ı sadece bir kere tetiklemek istenilebilir.Bu durumda once kullanilabilir.Tek sefer 

eventEmiter.once("merhaba", function () {
    console.log("Tek sefer");
})

eventEmiter.emit("merhaba");
eventEmiter.emit("merhaba");//burası çalışmayacaktır