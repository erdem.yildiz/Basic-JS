/**
 * Bir modül içersine yazılan fonksiyonlara başka bir sayfada çağırmak için aşağıdaki yapı kullanılabilir.
 */

function foo() {
    return 'bar';
}

function bar() {
    return 'foo';
}

module.exports.foo = foo;
module.exports.bar = bar;

// index.js sayfasından ulaşmak için

const fo = require("path");
const br = require("path");
//yada
const {f,b} = require("path");