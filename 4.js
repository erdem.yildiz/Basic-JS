/* 

    *for loop içerisnde var i tanımı sadece for scope'unda kalmayıp kodun
    devamıda'da kendinsini dahil etmektedir.For lop'dna sonra ekrana i basılırsa
    for'da kalan son değer ekrana basar

    *var ile aynı isimde birden fazla variable declare edilebilir fakat bunu let ile
    yaptığımızda ayı isimde birden fazla global scope'da variable tanımlamaz hatası
    alınacaktır.
    

    var b = 1;
    var b = 2;

    let a = 1;
    let a = 2;


*/

let name    = "Erdem";
let surname = "STAR"    

console.log(`Benim adım ${name} , soyadım ${surname}`);


const degerler = {
    deger1 : "1",
    deger2 : "2",
    deger3 : "3",
};
const {deger1 ,deger2} = degerler;
console.log(deger1);


let arr = [1,2,3,4,5];
console.log(...arr);


let arr2 = [arr,6,7,8,9]; // dizi içinde dii var
let arr3 = [...arr,6,7,8,9]; // tek dizi içersinde tüm elemanlar
console.log(arr2);
console.log(arr3);

const [d1,d2,...ar] = arr3; // ilk deger d1 , diğeri d2 en son olanı ise ar dizisine ekleniecek
console.log(...ar);//dizin tüm elemanlarını ekrana basmak için kullanılabilir.












