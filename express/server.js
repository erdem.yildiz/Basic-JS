/**
 * Web tabanlı projelerde express adlı kütüphane kullanılacaktır.Yüzlerce sayfayı kendi elle route etmek , yada okuyup
 * son kullanıcıya göstermek herzaman kolay olmayabiliyor.Bunun üstesinden gelmek için express adli kütüphane imdadımıza
 * koşoacaktır.
 */
const express = require("express");
const app = express();

app.set("view engine","pug");

app.get("/",function (req,res){
    res.render("index",{name:"Erdemstar",surname:"Şakcir"});
});

app.get("/home",function (req,res){
    res.render("home");
});

app.get("/contact",function (req,res){
    res.render("contact");
});



app.listen(4000,function () {
    console.log("Server çalışıyor");
});