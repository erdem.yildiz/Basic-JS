/**
 * streaming mantığıyla video file okuma => bu işlemi şöyle düşenebilir.Youtube'da bir video açtığında video'nun aşağı 
 * kısmında saniyenin yavaş yavaş dolduğunu ve dolan saniyeleri izleyebilidğini biliyorsun.Burada da aynuı mantıkl
 * göstermek istediğin video dosyayı karşı tarafa bir bütün olarak indirilip göstermektense parça parça indirilip 
 * gösterilir.Bu işlemin mimari tarafı'da bütününü indirip göstermekten daha az işlem ve performans harcar.
 */

const fs = require("fs");
const video = fs.createReadStream("mot.mp4"); // burada bir eventemiter olarak düşünlebilir.

let totalData = 0;
let progress = 0;

fs.stat("mot.mp4",function (err,data){ // ile dosya üzerinde işlemler yapılabilir
    if (err){
        throw err
    }
    totalData = data.size; // burada dosyanın total boyutunu aldık
});

video.on("data", function (chunk) { // video'nun başladığını ve data getirmesi için kullanılan event
    progress += chunk.length;
    console.log(`% ${Math.round((100*progress)/totalData)} kadar indirilmiştir`);

});

video.on("end",function (){ // tüm video datalarının alındığında tetiklenecektir
    console.log("veri gönderimi bitti");
});