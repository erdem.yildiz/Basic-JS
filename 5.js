const retFive = function (num) {
    return 5+num;
}

//arrow fuction
const retFour = (num) => {
    return 4+num;
}


//arrow fuction (Alının data doğrudan return ediliyrsa )
const retFour = (num) => 4+num;


console.log(retFive(Math.random(1,100)));
console.log(retFour(Math.random(1,100)));