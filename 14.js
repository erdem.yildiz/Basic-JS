/**
 * Read strem ile okunuan data'ları write stream kullanarak yazmaya çalışcapınız.
 */

const fs = require("fs");
const read = fs.createReadStream("mot.mp4");
const write = fs.createWriteStream("new.mp4");

read.setMaxListeners(1000);
read.on("data", function (chunk) {
    read.pipe(write);
});
write.on("finish",function (){
    console.log("Kopyalama işlemi bitti");
})