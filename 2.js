//callback function'lar devam

/*
    Nodejs'de fonksiyonlar sıralı bir şekilde çalışmazlar.Çalışması hızlı gerçekleşen
    fonksiyon'dan en yavaşa doğru çalışor.Sıralı bir şekilde çalıştırmak için callback
    fonksiyonlar kullanılabilir
*/

function birinci(callback) {
    setTimeout(function () {
        console.log("Birinici");
        callback(ucuncu);
    },1000);
}

function ikinci(callback) {
    setTimeout(function () {
        console.log("ikinci");
        callback();

    },2000);
}

function ucuncu() {
    console.log("ucuncu");
}


birinci(ikinci);
