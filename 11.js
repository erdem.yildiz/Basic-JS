/**
 * Üzerinizde çalıştığınız dosyanın hangi path'de olduğunu olduğunu dosya ismiyle öğrenmek için kullanılan bazı fonk 
 * Nodejs'in asenkron çalıştığınız biliryorsun bu nedemektir tam olarak.2 farklı işlemin arka arkaya yapıldığını farz et
    eğer ilk işlemin bitmesi bir süreç alıcaksa işlem başlatılır ve sonra nodejs diğer kodları çalıştırmaya devam eder.
    1,2,3 satır şeklinde gider , php mantığıyla ilk 1'e bakar bittiğinde 2'ye gider değil.
 */

//console.log(__filename); // dosya adıyla beraber döner
//console.log(__dirname) // dosya adının bulunduğu yeri verir


/**
 * Dosya içeriğini okuma ve yazma işlemleri için fs modulu kullanılır
 */

var fs = require('fs');


//asenkron bir şekilde dosya okuma işlemi yapar.İlk başta okuma işlemi yapılıar daha sonran eğer başarıylısa data'ya atılır
//değişse err içersinde hata fıraltırır
fs.readFile("file.txt" , function(err,data){
    if (err){
        throw err;
    }
    else{
        console.log(data.toString());
    }
});

//Aşağıdaki fonksiyonda nodejs üzerinde çalışan thread durdurulacaktır ve işlem bitmeden aşağıya geçmeyecektir
var data = fs.readFileSync("file2.txt","utf8");
console.log(data.toString());

//aşağıadki fonksiyon'larda callback kullanılmalfır.

fs.appendFile() // dosya yoksa oluşturur varsa devamı niteliğnide yazar
fs.writeFile() // dosya yoksa oluşturur varsa dosyanın üzerine yazar
fs.unlink() // dosya silmek için kullanılır